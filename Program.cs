﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class GameOfLife
    {
        private bool[,] tab;

        public void randomizeTab(int AliveProbablitiyPercent)
        {
            Random rand = new Random();
            for (int i = 0; i < tab.GetLength(0); i++)
                for (int j = 0; j < tab.GetLength(1); j++)
                {
                    int w = rand.Next(100);
                    if (w < AliveProbablitiyPercent) tab[i, j] = true;
                    else tab[i, j] = false; 
                }
        }

        public GameOfLife(int x, int y)
        {
            tab = new bool[x, y];
        }
        public GameOfLife(bool[,] tab)
        {
            this.tab = tab;
        }

        public void display()
        {
            for (int i=0; i<tab.GetLength(0); i++)
            {
                for (int j=0; j<tab.GetLength(1); j++)
                {
                    char ch;
                    if (tab[i,j]) ch = '*'; else ch = '.';
                    Console.Write(ch);
                }
                Console.Write('\n');
            }
            Console.WriteLine();
        }

        public int getNumNeighbours(int x, int y)
        {
            int result = 0;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (i == 0 && j == 0) continue;
                    int nX = x + i;
                    int nY = y + j;

                    if (nX >= 0 && nX < tab.GetLength(0) && nY >= 0 && nY < tab.GetLength(1))
                    {
                        if (tab[nX,nY])
                        {
                            result++;
                        }
                    }
                }
            }
            return result;
        }
        public void iterate()
        {
            bool[,] newTab = new bool[tab.GetLength(0), tab.GetLength(1)];
            for (int i = 0; i < tab.GetLength(0); i++)
            {
                for (int j = 0; j < tab.GetLength(1); j++)
                {
                    int nNeighbours = getNumNeighbours(i, j);
                    if (nNeighbours < 2 || nNeighbours > 3)
                    {
                        newTab[i, j] = false;
                    }
                    else if (nNeighbours == 2)
                    {
                        newTab[i, j] = tab[i, j];
                    }
                    else if (nNeighbours == 3)
                    {
                        newTab[i, j] = true;
                    }
                }
            }
            tab = newTab;
        }
    }

    class GameOfLifeMain
    {
        static void Main(string[] args)
        {
            //bool[,] temp = new bool[3, 3];
            //for (int i = 0; i < temp.GetLength(0); i++)
            //    temp[i, 1] = true;
            ////zabka
            //temp = new bool[6, 6];
            //temp[1, 2] = true;
            //temp[1, 3] = true;
            //temp[2, 1] = true;
            //temp[3, 4] = true;
            //temp[4, 2] = true;
            //temp[4, 3] = true;


            //GameOfLife TheGame = new GameOfLife(temp);
            GameOfLife TheGame = new GameOfLife(10,10);
            TheGame.randomizeTab(50);
            while (true)
            {
                Console.Clear();
                TheGame.display();
                Console.ReadKey();
                TheGame.iterate();
            }
            //nowaGra.iteruj();
        }
    }
}
